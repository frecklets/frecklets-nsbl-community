doc:
  short_help: Add an 'A' record to a dnsimple domain.
  examples:
    - title: Add a record for 'dev.cutecode.co'.
      vars:
        account_mail: my@example.com
        api_token: 123..xyz
        record: dev
        domain: cutecode.co
        ip: 123.123.123.123
args:
  api_token:
    doc:
      short_help: A dnsimple api token.
    type: string
    secret: true
    required: true
  account_email:
    doc:
      short_help: The dnsimple account email.
    type: string
    required: true
  domain:
    doc:
      short_help: The (main) domain name.
    type: string
    required: true
  record:
    doc:
      short_help: The sub-domain name to create (without the main domain name).
    type: string
  ip:
    doc:
      short_help: The ip address to point to.
    type: string
    required: true
  ttl:
    doc:
      short_help: The time to life of the record.
    type: integer
    required: false
frecklets:
  - frecklet:
      name: dnsimple
      type: ansible-module
      desc:
        short: Add dns A record to dnsimple.
      properties:
        internet: true
        elevated: false
        idempotent: true
      resources:
        python-package:
          - "dnsimple>=1.0.0"
    task:
      delegate_to: localhost
    vars:
      account_api_token: "{{:: api_token ::}}"
      account_email: "{{:: account_email ::}}"
      domain: "{{:: domain ::}}"
      record: "{{:: record ::}}"
      type: "A"
      value: "{{:: ip ::}}"
      ttl: "{{:: ttl ::}}"
      state: present
