doc:
  short_help: Create an instance on Amazon EC2, if it doesn't exist yet.

args:
  instance_id:
    doc:
      short_help: The instance id.
    type: string
    required: true
  aws_access_key:
    doc:
      short_help: The AWS access key.
    type: string
    required: true
  aws_secret_key:
    doc:
      short_help: The AWS secret key.
    type: string
    secret: true
    required: true
  wait:
    doc:
      short_help: Wait for the instance to be deleted.
      help: |
        Wait for the instance to be deleted.

        If set to 'false', no result will be registered as the details of this instance won't be known.
    type: boolean
    required: false
    default: true
  wait_timeout:
    doc:
      short_help: How long to wait (in seconds) for the instance to finish booting/terminating.
    type: integer
    required: false
    default: 600
  region:
    doc:
      short_help: The AWS region to use.
      references:
        "AWS region documentation": "https://docs.aws.amazon.com/general/latest/gr/rande.html#ec2_region"
    type: string
    required: true

frecklets:
  - frecklet:
      name: ec2_instance
      type: ansible-module
      properties:
        elevated: false
        idempotent: true
        internet: true
      resources:
        python-package:
          - boto
          - boto3
          - botocore
      desc:
        short: "delete EC2 instance: {{:: instance_id ::}}"
    task:
      include-type: include
      delegate_to: localhost
    vars:
      aws_access_key: "{{:: aws_access_key ::}}"
      aws_secret_key: "{{:: aws_secret_key ::}}"
      region: "{{:: region ::}}"
      state: absent
      instance_ids:
        - "{{:: instance_id ::}}"

